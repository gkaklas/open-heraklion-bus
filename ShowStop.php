<!DOCTYPE html>
<html>
<head>
    <title>OpenHB - ShowStop</title>
    <?php include 'modules/head.php'; ?>
    <?php include 'modules/map.php'; ?>
</head>
<body>
<?php include 'modules/nav.php'; ?>

<h1>ShowStop</h1>
<p>Below you can find two tables. One of them shows a list of routes tha visit 
this bus stop. Click on a route's name to see a list of its bus stops and the 
position of the bus on the map. The other table shows upcoming arrivals. There 
is also a map showing the location of the requested stop.</p>


<?php
include 'modules/libdata.php';

if (isValidStop($_GET["stop"])){
    $stop=getStopInfo($_GET["stop"]);
    echo '<header><h1>Stop ' . $stop['id'] . ' - ' . $stop['name'] . '</h1></header>';
    echo '<ul class="nav nav-tabs" role="tablist" >';
    echo '<li role="presentation" class="active"><a href="#routes" aria-controls="routes" data-toggle="tab" role="tab">Routes</a></li>';
    echo '<li role="presentation"><a href="#map" onclick="setTimeout(function(){map.updateSize();}, 200);" aria-controls="map" data-toggle="tab" role="tab">Map</a></li>';
    echo '</ul>';
    echo '<div class="tab-content">';
    echo '<div id="routes" class="tab-pane fade in active" role="tabpanel">';
    PrintRoutes($_GET["stop"]);
    echo '</div><div id="map" class="tab-pane fade" role="tabpanel">';
    ShowMap(getPositionOfApproachingStop($stop['id'],'go'),getPositionOfApproachingStop($stop['id'],'come'),array(array($stop['lat'],$stop['lon'])));
    echo '</div></div>';
} else {
    echo '<span style="display:inline-block" class="alert alert-danger" role="alert">No such stop. <a href="javascript:history.back()">Go back?</a></span>';
}

function PrintRoutes($stp){
    global $routes;
    echo '<table class="table table-striped table-bordered"><thead><tr>';
    echo '<th>Line #</th><th>Route Name</th><th>Direction</th>';
    echo '</tr></thead><tbody>';
    foreach ($routes as $route){
        foreach ($route['stops'] as $direction => $stops){
            foreach ($stops as $stop){
                if ($stop == $stp){
                    echo '<tr><td>' . $route['line'] . '</td>';
                    echo '<td><a href="ShowRoute.php?route=' .  $route['id'] . '">' . $route['name'] . '</a></td>';
                    echo '<td>' . getLabelOfDir($direction) . '</td></tr>';
                }
            }

        }
    }
    echo '</tbody></table>';
}
?>



<?php include 'modules/foot.php'; ?>
</body>
</html


