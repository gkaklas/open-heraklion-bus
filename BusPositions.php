<!DOCTYPE html>
<html>
<head>
    <title>OpenHB - Bus Positions</title>
    <?php include 'modules/head.php'; ?>
    <?php include 'modules/map.php'; ?>
    <style type="text/css">
        #mymap{
            height:100%;
            width:100%;
        }
    </style>
</head>
<body>
<?php include 'modules/nav.php'; ?>

<h1>Bus Positions</h1>
<p>This map displays the positions of every bus of every route.</p>

<?php
include 'modules/libdata.php';

$positionsRed = array();
$positionsGreen = array();
foreach ($routes as $route){
    foreach (BusPosition($route['id'], 'go') as $bus){
        array_push($positionsRed, array($bus['lat'], $bus['lon']));
    }
    foreach (BusPosition($route['id'], 'come') as $bus){
        array_push($positionsGreen, array($bus['lat'], $bus['lon']));
    }
}
ShowMap($positionsRed,$positionsGreen);

?>

</tbody>
</table>


<?php include 'modules/foot.php'; ?>
</body>
</html


