<!DOCTYPE html>
<html>
<head>
    <title>OpenHB - Lines</title>
    <?php include 'modules/head.php'; ?>
</head>
<body>
<?php include 'modules/nav.php'; ?>

<h1>Lines</h1>
<p>This is a table of all the unique line numbers.Click on a line number 
to see a list of routes that have this line number</p>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Line #</th>
        </tr>
    </thead>
<tbody>


<?php
include 'modules/libdata.php';

foreach (getLines() as $line){
    echo '<tr>';
    echo '<td><a href="ShowLine.php?line=' .  $line . '">' .  $line . '</a></td>';
    echo '</tr>';
}

?>

</tbody>
</table>


<?php include 'modules/foot.php'; ?>
</body>
</html


