<!DOCTYPE html>
<html>
<head>
    <title>OpenHB - Home</title>
    <?php include 'modules/head.php'; ?>
</head>
<body>
<?php include 'modules/nav.php'; ?>
    <h1>Home</h1>
    <p>Open Heraklion Bus is a web application that displays data regading buses, 
    routes and stops of public transport bus services in Heraklion. It uses  modern web 
    technologies and frameworks: HTML5, CSS3, Bootstrap and jQuery to display 
    a beautiful and simple website, PHP to generate the content displayed and 
    the JSON file format to store and read data.
    </p>
    <p>All data are parsed from the official website of the company behind the buses,
    <code>astiko-irakliou.gr</code>. Disclaimer: the developer
    of Open Heraklion Bus isn't affiliated in any way with Astiko Irakleiou or the development
    team of their website. The data displayed is copyrighted by Astiko Irakleiou and are copied
    from <code>astiko-irakliou.gr</code> -- I do not claim any 
    ownership of the data. I developed OpenHB for my own personal use and not as an official 
    or unofficial replacement of the original website <code>astiko-irakliou.gr</code>.
    I am not responsible for any invalid data provided.



<?php include 'modules/foot.php'; ?>
</body>
</html


