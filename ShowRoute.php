<!DOCTYPE html>
<html>
<head>
    <title>DataBus - ShowRoute</title>
    <?php include 'modules/head.php'; ?>
    <?php include 'modules/map.php'; ?>
</head>
<body>
<?php include 'modules/nav.php'; ?>

<h1>ShowRoute</h1>
<p>There are two tables below that list the bus stops of a route; one for each 
direction. Click on a bus stop's name to see a list of routes that visit it 
and view its position on a map.</p>



<?php
include 'modules/libdata.php';

if (isValidRoute($_GET['route'])){
    $route = getRouteInfo($_GET['route']);
    echo '<header><h1>' . $route['line'] . ' - ' . $route['name'] . '</h1></header>';
    echo '<ul class="nav nav-tabs" role="tablist" >';
    echo '<li role="presentation" class="active"><a href="#stops" aria-controls="stops" data-toggle="tab" role="tab">Stops</a></li>';
    echo '<li role="presentation"><a href="#map" onclick="setTimeout(function(){map.updateSize();}, 200);" aria-controls="map" data-toggle="tab" role="tab">Map</a></li>';
    echo '</ul>';
    echo '<div class="tab-content">';
    echo '<div id="routes" class="tab-pane fade in active" role="tabpanel">';
    PrintStops($route,'go');
    PrintStops($route,'come');
    echo '</div><div id="map" class="tab-pane fade" role="tabpanel">';
    ShowMap(getPositionOfRoute($route['id'],'go'),getPositionOfRoute($route['id'],'come'));
    echo '</div></div>';
} else {
    echo '<span style="display:inline-block" class="alert alert-danger" role="alert">No such route. <a href="javascript:history.back()">Go back?</a></span>';
}

function PrintStops($rt,$dir){
    echo '<table class="table table-striped table-bordered"><thead><tr>';
    echo '<th>Stop #</th><th>Stop Name</th>';
    echo '</tr></thead><tbody>';
    foreach ($rt['stops'][$dir] as $stp){
        $stop = getStopInfo($stp);
        echo '<tr><td>' . $stop['id'] . '</td>';
        echo '<td><a href="ShowStop.php?stop=' .  $stop['id'] . '">' . $stop['name'] . '</a></td></tr>';
    }
    echo '</tbody></table>';
}
?>



<?php include 'modules/foot.php'; ?>
</body>
</html


