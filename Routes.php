<!DOCTYPE html>
<html>
<head>
    <title>OpenHB - Routes</title>
    <?php include 'modules/head.php'; ?>
</head>
<body>
<?php include 'modules/nav.php'; ?>

<h1>Routes</h1>
<p>This is a table of all the routes. Click on a route's name to see a list of 
its bus stops and the position of the bus on the map. Click on a line number 
to see a list of routes that have this line number</p>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Line #</th>
            <th>Name</th>
        </tr>
    </thead>
<tbody>


<?php
include 'modules/libdata.php';

foreach ($routes as $route){
    echo '<tr>';
    echo '<td><a href="ShowLine.php?line=' .  $route['line'] . '">' .  $route['line'] . '</a></td>';
    echo '<td><a href="ShowRoute.php?route='  . $route['id'] . '">' . $route['name'] . '</a></td>';
    echo '</tr>';
}

?>

</tbody>
</table>


<?php include 'modules/foot.php'; ?>
</body>
</html


