<!DOCTYPE html>
<html>
<head>
    <title>OpenHB - Stops</title>
    <?php include 'modules/head.php'; ?>
</head>
<body>
<?php include 'modules/nav.php'; ?>

<h1>Stops</h1>
<p>This is a table of all the bus stops. Click on a stop's name to view its 
position on the map, a list of all the routes that visit it and the buses 
on their way to it.</p>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Stop #</th>
            <th>Stop Name</th>
        </tr>
    </thead>
<tbody>


<?php
include 'modules/libdata.php';

foreach ($stops as $stop){
    echo '<tr>';
    echo '<td>' .  $stop['id'] . '</td>';
    echo '<td><a href="ShowStop.php?stop='  . $stop['id'] . '">' . $stop['name'] . '</a></td>';
    echo '</tr>';
}

?>

</tbody>
</table>


<?php include 'modules/foot.php'; ?>
</body>
</html


