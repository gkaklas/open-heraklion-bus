<?php
include 'data.php';
include 'libher/libher.php';

function getStopsOfRoute($id){
    global $routes;
    foreach ($routes as $route){
        if ($route['id'] == $id){
            return $route['stops'];
        }
    }
}

function getRoutesOfLine($line){
    global $routes;
    $tmp = array();
    foreach ($routes as $route){
        if ($route['line'] == $line){
            array_push($tmp, $route);
        }
    }
    return $tmp;
}

function getRoutesByStop($stopId,$direction){
    global $routes;
    $res = array();
    foreach ($routes as $route){
        foreach ($route['stops'][$direction] as $stop){
            if ($stop == $stopId){
                array_push($res, $route["id"]);
            }
        }
    }
    return $res;
}

function isValidRoute($id){
    global $routes;
    foreach ($routes as $route){
        if ($route['id'] == $id){
            return true;
        }
    }
    return false;
}

function isValidLine($line){
    global $routes;
    foreach ($routes as $route){
        if ($route['line'] == $line){
            return true;
        }
    }
    return false;
}

function isValidStop($stp){
    global $stops;
    foreach ($stops as $stop){
        if ($stop['id'] == $stp){
            return true;
        }
    }
    return false;
}


function getRouteInfo($id){
    global $routes;
    foreach ($routes as $route){
        if ($route['id'] == $id){
            return $route;
        }
    }
    return false;
}

function getStopInfo($id){
    global $stops;
    foreach ($stops as $stop){
        if ($stop['id'] == $id){
            return $stop;
        }
    }
    return false;
}

function getLabelOfDir($dir){
    if ($dir == 'go'){
        return '<span class="label label-info">Go</span>';
    } else if ($dir == 'come'){
        return '<span class="label label-primary">Come</span>';
    } else {
        return false;
    }
}

function getLines(){
    global $routes;
    $lines = array();
    foreach ($routes as $r){
        $found = false;
        foreach($lines as $l){
            if($r["line"] == $l){
                $found=true;
            }
        }
        if(!$found){
            array_push($lines, $r["line"]);
        }
    }
    return $lines;
}

function getPositionOfRoute($rt,$direction){
    $locations = array();
    foreach (BusPosition($rt,$direction) as $bus){
        array_push($locations,array($bus["lat"],$bus["lon"]));
    }
    return $locations;
}

function getPositionOfLine($line,$direction){
    $locations = array();
    foreach(getRoutesOfLine($line) as $route){
        foreach(getPositionOfRoute($route["id"],$direction) as $pos){
            array_push($locations,$pos);
        }
    }
    return $locations;
}

function getPositionOfApproachingStop($stop,$direction){
    $res = array();
    foreach(getRoutesByStop($stop,$direction) as $route){
        foreach(getPositionOfRoute($route,$direction) as $bus){ 
            array_push($res, $bus);
        }
    }
    return $res;
}

?>
