<?php
$lang = 'en';

function DownloadUrl($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}
function DownloadRouteUIDs(){
    global $lang;
    $codes = array();
    $dom = new DomDocument();
    @$dom->loadHTML(DownloadUrl('http://astiko-irakleiou.gr/#' . $lang . '/'));

    foreach ($dom->getElementsByTagName('select')[0]->childNodes as $option){
        $id = $option->getAttribute('value');
        if ($id == 0){
            continue;
        }
        array_push($codes, $id);
    }
    return $codes;
}

function DownloadRouteTitle($id){
    global $lang;
    $title = array();
    $dom = new DomDocument();
    @$dom->loadHTML(spleet(DownloadUrl('http://astiko-irakleiou.gr/' . $lang . '/routeinfo/list/' . $id . '/?a=1')));
    $t= utf8_decode($dom->getElementsByTagName('h1')[0]->nodeValue);
    $pieces = explode(':',$t);
    $title[0]=array_reverse(explode(' ', $pieces[0]))[0];
    $title[1]=$pieces[1];

    return $title;

}

function DownloadStopName($stp){
    global $lang;
    return DownloadUrl('http://astiko-irakleiou.gr/' . $lang . '/stopinfo/getname/' . $stp . '?a=1');
}

function DownloadRouteStops($line,$dir){
    global $lang;
    if ($dir == 'go'){
        $var='a';
    } else if ($dir == 'come'){
        $var = 'b';
    }

    $pieces = explode('/',$line);
    $stopString = $pieces[0] . '/' . $pieces[1] . '/' . $var . '/';
    $html = new DomDocument();
    @$html->loadHTML(DownloadUrl('http://astiko-irakleiou.gr/' . $lang . '/stopinfo/route/' . $stopString . '?a=1'));
    return $html->getElementsByTagName('option');
}

function DownloadStopInfo($stp){
    global $lang;
    return spleet(DownloadUrl('http://astiko-irakleiou.gr/' . $lang . '/stopinfo/screen/' . $stp . '?a=1'));
}


function DownloadBusposition($line,$direction){
    global $lang;
    $b = spleet(DownloadUrl('http://astiko-irakleiou.gr/' . $lang . '/buspositions/' . $direction . '/' . $line . '?a=1'));
    $b = preg_replace('/"/i', '', $b);
    $b = explode('||', $b);
    return $b;

}

function spleet($mrkr) {
    $z = $mrkr . "";
    $z = preg_replace("/grZNn/i", '=', $z);
    $z = base64_decode($z);
    $z = preg_replace("/!/i", '3', $z);
    $z = base64_decode($z);
    return $z;
}
?>
