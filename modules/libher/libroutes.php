<?php
function Routes(){
    global $lang;
    $GenRoutes = array();
    $GenRoutes_keys = array('id', 'line', 'name', 'stops');
    $GenRoutesStop_keys = array('go', 'come');
    $codes = DownloadRouteUIDs();


    # Get each route's name
    foreach ($codes as $id){
        $GenRoutes_values = array();
        $GenRoutesStop_values = array();
        $title = DownloadRouteTitle($id);
        $lineCode = $title[0];
        $lineName = $title[1];

        array_push($GenRoutesStop_values, StopsOfRoute($id,'go'), StopsOfRoute($id,'come'));
        array_push($GenRoutes_values, $id, $lineCode, $lineName, array_combine($GenRoutesStop_keys, $GenRoutesStop_values));
        array_push($GenRoutes, array_combine($GenRoutes_keys, $GenRoutes_values));
    }
    return $GenRoutes;
}


function StopsOfRoute($line,$dir){
    global $lang;

    $stopsList = array();
    foreach (DownloadRouteStops($line,$dir) as $stop){
        $stp = $stop->getAttribute('value');
        if ($stp == 0){
            continue;
        }
        array_push($stopsList, $stp);
    }
    return $stopsList;
}

?>
