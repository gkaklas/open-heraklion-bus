<?php

function BusPosition($routeid, $direction){
    global $lang;
    $buspositions = DownloadBusposition($routeid,$direction);

    $GenBusPositions = array();
    $GenBusPositions_keys = array('routeid', 'id', 'lat', 'lon', 'direction');

    foreach ($buspositions as $bp){
        $GenBusPositions_values = array();

        if ($bp == ''){
            break;
        }

        $pieces = explode('-', $bp);

        $id = $pieces[0];
        if ($id != 'null'){
            $lat = $pieces[2];
            $lon = $pieces[1];

            array_push($GenBusPositions_values, $routeid, $id, $lat, $lon, $direction);
            array_push($GenBusPositions, array_combine($GenBusPositions_keys, $GenBusPositions_values));
        }
    }
    return $GenBusPositions;
}

?>
