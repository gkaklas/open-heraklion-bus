<link rel="stylesheet" href="openlayers/ol.css" />
<script src="openlayers/ol.js"></script>
<style type="text/css">
    #mymap{
        height:300px;
        width:300px;
        overflow:hidden;
    }
</style>

<?php
function ShowMap($coordsRed,$coordsGreen,$coordsBlue){
    if(count($coordsRed)>0 || count($coordsGreen)>0){
?>
    <div id="mymap"></div>
    <script>
        var pos = new ol.proj.fromLonLat([25.1329,35.3342]);
        var features = [];
<?php 
    if(count($coordsRed)>0){
echo "var coordinatesRed = [";
for ($i=0; $i<count($coordsRed)-1;$i++){
    $loc = $coordsRed[$i];
    echo '['. $loc[1] . ',' . $loc[0] . '],';
}
echo '['. $coordsRed[$i][1] . ',' . $coordsRed[$i][0] . ']];';
?>
        var styleRed = new ol.style.Style({
            image: new ol.style.Circle({
                fill: new ol.style.Fill({color: '#FF0000'}),
                stroke: new ol.style.Stroke({color: '#000000'}),
                radius: 5
            })
        });
        for(var i=0; i<coordinatesRed.length; i++){
            var feat = new ol.Feature({
                geometry: new ol.geom.Point(new ol.proj.fromLonLat([coordinatesRed[i][0],coordinatesRed[i][1]]))
            });
            feat.setStyle(styleRed);
            features.push(feat);
        }

<?php
}
    if(count($coordsGreen)>0){
echo "var coordinatesGreen = [";
for ($i=0; $i<count($coordsGreen)-1;$i++){
    $loc = $coordsGreen[$i];
    echo '['. $loc[1] . ',' . $loc[0] . '],';
}
echo '['. $coordsGreen[$i][1] . ',' . $coordsGreen[$i][0] . ']];';
?>
        var styleGreen = new ol.style.Style({
            image: new ol.style.Circle({
                fill: new ol.style.Fill({color: '#00FF00'}),
                stroke: new ol.style.Stroke({color: '#000000'}),
                radius: 5
            })
        });
        for(var i=0; i<coordinatesGreen.length; i++){
            var feat = new ol.Feature({
                geometry: new ol.geom.Point(new ol.proj.fromLonLat([coordinatesGreen[i][0],coordinatesGreen[i][1]]))
            });
            feat.setStyle(styleGreen);
            features.push(feat);
        }
<?php
}
    if(count($coordsBlue)>0){
echo "var coordinatesBlue = [";
for ($i=0; $i<count($coordsBlue)-1;$i++){
    $loc = $coordsBlue[$i];
    echo '['. $loc[1] . ',' . $loc[0] . '],';
}
echo '['. $coordsBlue[$i][1] . ',' . $coordsBlue[$i][0] . ']];';
?>
        var styleBlue = new ol.style.Style({
            image: new ol.style.Circle({
                fill: new ol.style.Fill({color: '#0000FF'}),
                stroke: new ol.style.Stroke({color: '#000000'}),
                radius: 5
            })
        });
        for(var i=0; i<coordinatesBlue.length; i++){
            var feat = new ol.Feature({
                geometry: new ol.geom.Point(new ol.proj.fromLonLat([coordinatesBlue[i][0],coordinatesBlue[i][1]]))
            });
            feat.setStyle(styleBlue);
            features.push(feat);
        }
<?php
}
?>
        var vectorSource = new ol.source.Vector({features:features});
        var vectorLayer = new ol.layer.Vector({
            source: vectorSource,
            opacity: 0.8
        });
        var map = new ol.Map({
            target: document.getElementById("mymap"),
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM({
                        url: 'http://a.tile2.opencyclemap.org/transport/{z}/{x}/{y}.png',
                        attributions: [new ol.Attribution({
                            html: 'Maps © <a href="http://www.thunderforest.com">Thunderforest</a>, Data © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>'
                        })]
                    })
                }),
                vectorLayer
            ],
            view: new ol.View({
                center: pos,
                zoom:13
            })
        });
    </script>
<?php
    }else{
?>
    <span style="display:inline-block" class="alert alert-danger" role="alert">No information to display on the map yet (probably no bus is on its way). <a href="javascript:history.back()">Go back?</a></span>

<?php
    }
}
?>
