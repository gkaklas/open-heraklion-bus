<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" aria-expanded="false" aria-controls="navbar" data-target=".navbar-collapse" data-toggle="collapse" type="buttton">
                <span class="sr-only">Toggle navigation bar</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href=".">Open Heraklion Bus</a>
        </div>
        <div class="navbar-collapse collapse" >
            <ul class="nav navbar-nav">
                <li class="<?php echo isActive("Routes.php");?>" ><a href="Routes.php">Routes</a></li>
                <li class="<?php echo isActive("Lines.php");?>" ><a href="Lines.php">Lines</a></li>
                <li class="<?php echo isActive("Stops.php");?>"><a href="Stops.php">Stops</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">

<?php

function isActive($url){
    if (basename($_SERVER['PHP_SELF']) == $url){
        return ' active ';
    }
    return;
}

?>
