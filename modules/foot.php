</div>
<footer class="footer">
<div class="container text-center">
    <p class="text-muted">
        Made by George Kaklamanos (@gkaklas). 
        <a href="https://gitlab.com/gkaklas/open-heraklion-bus">Source code</a>. Open Heraklion Bus respects the users' freedoms.
        <img src="img/agplv3-88x31.png" />
    </p>
</div>
</footer>
