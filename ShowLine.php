<!DOCTYPE html>
<html>
<head>
    <title>OpenHB - ShowLine</title>
    <?php include 'modules/head.php'; ?>
    <?php include 'modules/map.php'; ?>
</head>
<body>
<?php include 'modules/nav.php'; ?>

<h1>ShowLine</h1>
<p>Here you can find a list of routes that have a specified line number. Click 
on a route's name to see a list of its bus stops and the position of the bus 
on the map.</p>



<?php
include 'modules/libdata.php';

if (isValidLine($_GET["line"])){
    echo '<header><h1>Line ' . $_GET["line"] . '</h1></header>';
    PrintRoutes($_GET["line"]);
} else {
    echo '<span style="display:inline-block" class="alert alert-danger" role="alert">No such line. <a href="javascript:history.back()">Go back?</a></span>';
}

function PrintRoutes($line){
    echo '<ul class="nav nav-tabs" role="tablist" >';
    echo '<li role="presentation" class="active"><a href="#routes" aria-controls="routes" data-toggle="tab" role="tab">Routes</a></li>';
    echo '<li role="presentation"><a href="#map" onclick="setTimeout(function(){map.updateSize();}, 200);" aria-controls="map" data-toggle="tab" role="tab">Map</a></li>';
    echo '</ul>';
    echo '<div class="tab-content">';
    echo '<div id="routes" class="tab-pane fade in active" role="tabpanel">';
    $routes = getRoutesOfLine($line);
    echo '<table class="table table-striped table-bordered"><thead><tr>';
    echo '<th>Route #</th><th>Route Name</th>';
    echo '</tr></thead><tbody>';
    foreach ($routes as $route){
        echo '<tr><td>' . $route['id'] . '</td>';
        echo '<td><a href="ShowRoute.php?route=' .  $route['id'] . '">' . $route['name'] . '</a></td></tr>';
    }
    echo '</tbody></table>';
    echo '</div><div id="map" class="tab-pane fade" role="tabpanel">';
    ShowMap(getPositionOfLine($line,'go'),getPositionOfLine($line,'come'));
    echo '</div></div>';
}
?>



<?php include 'modules/foot.php'; ?>
</body>
</html


